package grpc

import (
	"gitlab.com/market93934/market_go_arrival_service.go/config"
	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc/client"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc/service"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/logger"
	"gitlab.com/market93934/market_go_arrival_service.go/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	arrival_service.RegisterArrivalServiceServer(grpcServer, service.NewArrivalService(cfg, log, strg, srvc))
	arrival_service.RegisterArrivalProductServiceServer(grpcServer, service.NewArrivalProductService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
