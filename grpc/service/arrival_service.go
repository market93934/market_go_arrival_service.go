package service

import (
	"context"

	"gitlab.com/market93934/market_go_arrival_service.go/config"
	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc/client"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/logger"
	"gitlab.com/market93934/market_go_arrival_service.go/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type arrivalService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*arrival_service.UnimplementedArrivalServiceServer
}

func NewArrivalService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *arrivalService {
	return &arrivalService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *arrivalService) Create(ctx context.Context, req *arrival_service.CreateArrival) (resp *arrival_service.Arrival, err error) {

	i.log.Info("---CreateArrival------>", logger.Any("req", req))

	pKey, err := i.strg.Arrival().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateArrival->Arrival->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	
	resp, err = i.strg.Arrival().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyArrival->Arrival->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
func (i *arrivalService) GetByID(ctx context.Context, req *arrival_service.ArrivalPrimaryKey) (resp *arrival_service.Arrival, err error) {

	i.log.Info("---GetArrivalByID------>", logger.Any("req", req))

	resp, err = i.strg.Arrival().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArrivalByID->Arrival->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *arrivalService) GetList(ctx context.Context, req *arrival_service.GetListArrivalRequest) (*arrival_service.GetListArrivalResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Arrival().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListArrival->Arrival->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *arrivalService) Update(ctx context.Context, req *arrival_service.UpdateArrival) (resp *arrival_service.Arrival, err error) {
	i.log.Info("---UpdateArrival------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Arrival().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateArrival--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Arrival().GetByPKey(ctx, &arrival_service.ArrivalPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetArrival->Arrival->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *arrivalService) Delete(ctx context.Context, req *arrival_service.ArrivalPrimaryKey) (resp *arrival_service.ArrivalEmpty, err error) {

	i.log.Info("---DeleteArrival------>", logger.Any("req", req))

	resp, err = i.strg.Arrival().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteArrival->Arrival->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
