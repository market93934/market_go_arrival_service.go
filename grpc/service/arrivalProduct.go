package service

import (
	"context"

	"gitlab.com/market93934/market_go_arrival_service.go/config"
	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc/client"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/logger"
	"gitlab.com/market93934/market_go_arrival_service.go/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type arrivalProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*arrival_service.UnimplementedArrivalProductServiceServer
}

func NewArrivalProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *arrivalProductService {
	return &arrivalProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *arrivalProductService) Create(ctx context.Context, req *arrival_service.CreateArrivalProduct) (resp *arrival_service.ArrivalProduct, err error) {

	i.log.Info("---CreateArrivalProduct------>", logger.Any("req", req))

	pKey, err := i.strg.ArrivalProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateArrivalProduct->ArrivalProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ArrivalProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyArrivalProduct->ArrivalProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *arrivalProductService) GetByID(ctx context.Context, req *arrival_service.ArrivalProductPrimaryKey) (resp *arrival_service.ArrivalProduct, err error) {

	i.log.Info("---GetArrivalProductByID------>", logger.Any("req", req))

	resp, err = i.strg.ArrivalProduct().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArrivalProductByID->ArrivalProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *arrivalProductService) GetList(ctx context.Context, req *arrival_service.GetListArrivalProductRequest) (*arrival_service.GetListArrivalProductResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.ArrivalProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListArrivalProduct->ArrivalProduct->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *arrivalProductService) Update(ctx context.Context, req *arrival_service.UpdateArrivalProduct) (resp *arrival_service.ArrivalProduct, err error) {
	i.log.Info("---UpdateArrivalProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ArrivalProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateArrivalProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ArrivalProduct().GetByPKey(ctx, &arrival_service.ArrivalProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetArrivalProduct->ArrivalProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *arrivalProductService) Delete(ctx context.Context, req *arrival_service.ArrivalProductPrimaryKey) (resp *arrival_service.ArrivalProductEmpty, err error) {

	i.log.Info("---DeleteArrivalProduct------>", logger.Any("req", req))

	resp, err = i.strg.ArrivalProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteArrivalProduct->ArrivalProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
