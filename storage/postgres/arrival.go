package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/helper"
)

type arrivalRepo struct {
	db *pgxpool.Pool
}

func NewArrivalRepo(db *pgxpool.Pool) *arrivalRepo {
	return &arrivalRepo{
		db: db,
	}
}

func (c *arrivalRepo) Create(ctx context.Context, req *arrival_service.CreateArrival) (resp *arrival_service.ArrivalPrimaryKey, err error) {
	arrival, err := c.GetAll(ctx, &arrival_service.GetListArrivalRequest{})
	if err != nil {
		return nil, err
	}
	var (
		id         = uuid.New()
		arrival_id = helper.GenerateString("A", int(arrival.Count)+1)
	)
	query := `INSERT INTO "arrival" (
			id,
			branch_id,
			provider_id,
			arrival_id
			) VALUES ($1, $2, $3, $4)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.ProviderId,
		arrival_id,
	)
	if err != nil {
		return nil, err
	}

	return &arrival_service.ArrivalPrimaryKey{Id: id.String()}, nil

}

func (c *arrivalRepo) GetByPKey(ctx context.Context, req *arrival_service.ArrivalPrimaryKey) (resp *arrival_service.Arrival, err error) {
	query := `
		SELECT
			id,
			branch_id,
			provider_id,
			status,
			arrival_id,
			created_at,
			updated_at
		FROM "arrival"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branch_id   sql.NullString
		provider_id sql.NullString
		status      sql.NullString
		arrival_id  sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&provider_id,
		&status,
		&arrival_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &arrival_service.Arrival{
		Id:         id.String,
		BranchId:   branch_id.String,
		ProviderId: provider_id.String,
		Status:     status.String,
		ArrivaId:   arrival_id.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *arrivalRepo) GetAll(ctx context.Context, req *arrival_service.GetListArrivalRequest) (resp *arrival_service.GetListArrivalResponse, err error) {

	resp = &arrival_service.GetListArrivalResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			provider_id,
			status,
			arrival_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "arrival"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchArrivalId != "" {
		filter += ` AND arrival_id ILIKE '%' || '` + req.SearchArrivalId + `' || '%'`
	}
	if req.SearchBranchId != "" {
		filter += ` AND branch_id ILIKE '%' || '` + req.SearchBranchId + `' || '%'`
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branch_id   sql.NullString
			provider_id sql.NullString
			status      sql.NullString
			arrival_id  sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&provider_id,
			&status,
			&arrival_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Arrivals = append(resp.Arrivals, &arrival_service.Arrival{
			Id:         id.String,
			BranchId:   branch_id.String,
			ProviderId: provider_id.String,
			Status:     status.String,
			ArrivaId:   arrival_id.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *arrivalRepo) Update(ctx context.Context, req *arrival_service.UpdateArrival) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "arrival"
			SET
				branch_id = :branch_id,
				provider_id = :provider_id,
				status = :status,
				arrival_product_id = :arrival_product_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"provider_id": req.GetProviderId(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *arrivalRepo) Delete(ctx context.Context, req *arrival_service.ArrivalPrimaryKey) (*arrival_service.ArrivalEmpty, error) {

	query := `DELETE FROM "arrival" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &arrival_service.ArrivalEmpty{}, nil
}
