package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_arrival_service.go/config"
	"gitlab.com/market93934/market_go_arrival_service.go/storage"
)

type Store struct {
	db             *pgxpool.Pool
	arrival        storage.ArrivalRepoI
	arrivalProduct storage.ArrivalProductRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Arrival() storage.ArrivalRepoI {
	if s.arrival == nil {
		s.arrival = NewArrivalRepo(s.db)
	}
	return s.arrival
}

func (s *Store) ArrivalProduct() storage.ArrivalProductRepoI {
	if s.arrivalProduct == nil {
		s.arrivalProduct = NewArrivalProductRepo(s.db)
	}
	return s.arrivalProduct
}
