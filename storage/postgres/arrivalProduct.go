package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/helper"
)

type arrivalProductRepo struct {
	db *pgxpool.Pool
}

func NewArrivalProductRepo(db *pgxpool.Pool) *arrivalProductRepo {
	return &arrivalProductRepo{
		db: db,
	}
}

func (c *arrivalProductRepo) Create(ctx context.Context, req *arrival_service.CreateArrivalProduct) (resp *arrival_service.ArrivalProductPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "arrival_product" (
			id,
			category_id,
			brand_id,
			bar_code,
			count,
			price,
			arrival_id,
			product_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.CategoryId,
		req.BrandId,
		req.BarCode,
		req.Count,
		req.Price,
		req.ArrivalId,
		req.ProductId,
	)
	if err != nil {
		return nil, err
	}

	return &arrival_service.ArrivalProductPrimaryKey{Id: id.String()}, nil

}

func (c *arrivalProductRepo) GetByPKey(ctx context.Context, req *arrival_service.ArrivalProductPrimaryKey) (resp *arrival_service.ArrivalProduct, err error) {
	query := `
		SELECT
			id,
			category_id,
			brand_id,
			bar_code,
			count,
			price,
			arrival_id,
			product_id,
			created_at,
			updated_at
		FROM "arrival_product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		bar_code    sql.NullString
		count       int64
		price       float64
		arrival_id  sql.NullString
		product_id  sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&brand_id,
		&bar_code,
		&count,
		&price,
		&arrival_id,
		&product_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &arrival_service.ArrivalProduct{
		Id:         id.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		BarCode:    bar_code.String,
		Count:      count,
		Price:      price,
		ArrivalId:  arrival_id.String,
		ProductId:  product_id.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *arrivalProductRepo) GetAll(ctx context.Context, req *arrival_service.GetListArrivalProductRequest) (resp *arrival_service.GetListArrivalProductResponse, err error) {

	resp = &arrival_service.GetListArrivalProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			category_id,
			brand_id,
			bar_code,
			count,
			price,
			arrival_id,
			product_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "arrival_product"
	`
	// SearchBrandId    string `protobuf:"bytes,3,opt,name=searchBrandId,proto3" json:"searchBrandId,omitempty"`
    // SearchCategoryId string `protobuf:"bytes,4,opt,name=searchCategoryId,proto3" json:"searchCategoryId,omitempty"`
    // SearchBarCode
	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchBrandId != "" {
		filter += ` AND brand_id ILIKE '%' || '` + req.SearchBrandId + `' || '%'`
	}
	if req.SearchCategoryId != "" {
		filter += ` AND category_id ILIKE '%' || '` + req.SearchCategoryId + `' || '%'`
	}
	if req.SearchBarCode != "" {
		filter += ` AND bar_code ILIKE '%' || '` + req.SearchBarCode + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			bar_code    sql.NullString
			count       int64
			price       float64
			arrival_id  sql.NullString
			product_id  sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&brand_id,
			&bar_code,
			&count,
			&price,
			&arrival_id,
			&product_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ArrivalProducts = append(resp.ArrivalProducts, &arrival_service.ArrivalProduct{
			Id:         id.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			BarCode:    bar_code.String,
			Count:      count,
			Price:      price,
			ArrivalId:  arrival_id.String,
			ProductId:  product_id.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *arrivalProductRepo) Update(ctx context.Context, req *arrival_service.UpdateArrivalProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "arrival_product"
			SET
				category_id = :category_id,
				brand_id = :brand_id,
				bar_code = :bar_code,
				count = :count,
				price = :price,
				arrival_id = :arrival_id,
				product_id = :product_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"bar_code":    req.GetBarCode(),
		"count":       req.GetCount(),
		"price":       req.GetPrice(),
		"arrival_id":  req.GetArrivalId(),
		"product_id":  req.GetProductId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *arrivalProductRepo) Delete(ctx context.Context, req *arrival_service.ArrivalProductPrimaryKey) (*arrival_service.ArrivalProductEmpty, error) {

	query := `DELETE FROM "arrival_product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &arrival_service.ArrivalProductEmpty{}, nil
}
