package storage

import (
	"context"

	"gitlab.com/market93934/market_go_arrival_service.go/genproto/arrival_service"
)

type StorageI interface {
	CloseDB()
	Arrival() ArrivalRepoI
	ArrivalProduct() ArrivalProductRepoI
}

type ArrivalRepoI interface {
	Create(context.Context, *arrival_service.CreateArrival) (*arrival_service.ArrivalPrimaryKey, error)
	GetByPKey(context.Context, *arrival_service.ArrivalPrimaryKey) (*arrival_service.Arrival, error)
	GetAll(context.Context, *arrival_service.GetListArrivalRequest) (*arrival_service.GetListArrivalResponse, error)
	Update(context.Context, *arrival_service.UpdateArrival) (int64, error)
	Delete(context.Context, *arrival_service.ArrivalPrimaryKey) (*arrival_service.ArrivalEmpty, error)
}

type ArrivalProductRepoI interface {
	Create(context.Context, *arrival_service.CreateArrivalProduct) (*arrival_service.ArrivalProductPrimaryKey, error)
	GetByPKey(context.Context, *arrival_service.ArrivalProductPrimaryKey) (*arrival_service.ArrivalProduct, error)
	GetAll(context.Context, *arrival_service.GetListArrivalProductRequest) (*arrival_service.GetListArrivalProductResponse, error)
	Update(context.Context, *arrival_service.UpdateArrivalProduct) (int64, error)
	Delete(context.Context, *arrival_service.ArrivalProductPrimaryKey) (*arrival_service.ArrivalProductEmpty, error)
}
