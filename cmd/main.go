package main

import (
	"context"
	"net"

	"gitlab.com/market93934/market_go_arrival_service.go/config"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc"
	"gitlab.com/market93934/market_go_arrival_service.go/grpc/client"
	"gitlab.com/market93934/market_go_arrival_service.go/pkg/logger"
	"gitlab.com/market93934/market_go_arrival_service.go/storage/postgres"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	// connect to db
	pgStore, err := postgres.NewPostgres(context.Background(), cfg)
	if err != nil {
		log.Panic("postgres.NewPostgres", logger.Error(err))
	}
	defer pgStore.CloseDB()

	// connect to services
	svrcs, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Panic("client.NewGrpcClients", logger.Error(err))
	}

	grpcServer := grpc.SetUpServer(cfg, log, pgStore, svrcs)

	lis, err := net.Listen("tcp", cfg.ARRIVALGRPCPort)
	if err != nil {
		log.Panic("net.Listen", logger.Error(err))
	}

	log.Info("GRPC: Server being started...", logger.String("port", cfg.ARRIVALGRPCPort))

	if err := grpcServer.Serve(lis); err != nil {
		log.Panic("grpcServer.Serve", logger.Error(err))
	}

}
